const path = require('path')
// const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
var env = process.env.WEBPACK_ENV

process.env.NODE_ENV = env

module.exports = {
  entry: {
    app: './docs/main.js'
  },

  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: '/build/'
  },

  devServer: {
    host: '0.0.0.0',
    port: 8000,
    inline: true
  },

  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'docs'),
      Lib: path.resolve(__dirname, 'src')
    }
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.s(c|a)ss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(svg|png|jpe?g|gif|woff|eot|ttf|woff2)$/i,
        use: ['file-loader']
      }
    ]
  },

  plugins: [
    new VueLoaderPlugin()
  ]
}
