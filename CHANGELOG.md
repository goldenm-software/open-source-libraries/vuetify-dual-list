# Changelog
## Version 3.0.0
- Fixed errors related to core-js dependency

## Version 2.0.3
- Fixed issue with "select all" button

## Version 2.0.2
- Fixed issue related to items array update

## Version 2.0.1
- Updated README.md

## Version 2.0.0
- Migrated to Vuetify v2.2.21
- General improvements
- Redesigned internal algorithms
- v-model support

## Version 1.0.6
- Bug fixed
- Props changed
- Vuetify 2 Arcadia support

## Version 1.0.5
- Bug fixed

## Version 1.0.4
- Bug fixed

## Version 1.0.3
- Pre Selected field
- Bug fixed

## Version 1.0.2
- Dense Mode
- Dark Mode
- Bug fixed

## Version 1.0.1
- Bug fixed

## Version 1.0.0
- Version launch