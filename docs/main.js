import Vue from 'vue'
import App from './App.vue'

// Main imports
import Vuetify from 'vuetify'

// // Vuetify locales
import es from 'vuetify/es5/locale/es'
import '@/assets/app.scss'

const colors = {
  primary: '#004e87',
  secondary: '#023a61',
  accent: '#4b4846'
}
// Create component
Vue.use(Vuetify)
const vuetify = new Vuetify({
  lang: {
    locales: { es },
    current: 'es'
  },

  icons: {
    iconfont: 'mdi',
    values: {
      cancel: 'mdi-close',
      prev: 'mdi-chevron-left',
      next: 'mdi-chevron-right'
    }
  },

  theme: {
    dark: false,
    themes: {
      dark: colors,
      light: colors
    },

    options: {
      customProperties: true
    }
  }
})

Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
