# Vuetify Dual-List
[![npm version](https://badge.fury.io/js/%40goldenm%2Fvuetify-dual-list.svg)](https://badge.fury.io/js/%40goldenm%2Fvuetify-dual-list)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/goldenm-software/open-source-libraries/vuetify-dual-list/blob/master/LICENSE)

## Online Demo
For online demo, please go to [this link](https://goldenm-software.gitlab.io/open-source-libraries/vuetify-dual-list/)

## Installation
```bash
npm install --save @goldenm/vuetify-dual-list
```
or 
```bash
yarn add @goldenm/vuetify-dual-list
```

## Example usage
```javascript
<template>
  <vuetify-dual-list
    v-model="selected"
    :items="items"
    item-text="text"
    item-value="value"
    mobile-breakpoint="960"
    :messages="messages"
    :icons="icons"
  />
</template>

<script>
export default {
  data () {
    return {
      items: [{ text: 'Item 1', value: 1 }, { text: 'Item 2', value: 2 }, { text: 'Item 3', value: 3 }],

      selected: [],

      messages: {
        listOptions: 'Options',
        listSelected: 'Selected',
        forwardAll: 'Toggle all to selected',
        forward: 'Toggle to selected',
        backwardAll: 'Toggle all to options',
        backward: 'Toggle to options',
        find: 'Find'
      },

      icons: {
        mobile: {
          forwardAll: 'mdi-chevron-double-down',
          forward: 'mdi-chevron-down',
          backwardAll: 'mdi-chevron-double-up',
          backward: 'mdi-chevron-up'
        },
        desktop: {
          forwardAll: 'mdi-chevron-double-right',
          forward: 'mdi-chevron-right',
          backwardAll: 'mdi-chevron-double-left',
          backward: 'mdi-chevron-left'
        }
      },
    }
  }
}
</script>
```
### Props
| Prop              | Description                                  | Default value                                                                                                                                                                                                                                                                                                 | Type            |
|-------------------|----------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
| items             | Items to display in the first list of items  | []                                                                                                                                                                                                                                                                                                            | Array           |
| item-text         | Text to display in lists                     | text                                                                                                                                                                                                                                                                                                          | String          |
| item-value        | Value used to sort items in lists            | value                                                                                                                                                                                                                                                                                                         | String          |
| mobile-breakpoint | Size to change to a Mobile view distribution | 960                                                                                                                                                                                                                                                                                                           | String | Number |
| messages          | Translations                                 | See default value below                                                                                                  | Object          |
| icons             | Icons used in arrows                         | See default value below | Object          |

### Slots
| Slot     | Description                                                          | Parameters                                             |
|----------|----------------------------------------------------------------------|--------------------------------------------------------|
| option   | Option list display slot. (Is inside of <v-list-item> vuetify tag)   | { item: Object // Item in your items prop (Iterated) } |
| selected | Selected list display slot. (Is inside of <v-list-item> vuetify tag) | { item: Object // Item in your items prop (Iterated) } |

### Default messages and icons
For icons, we use [Material Design Icons](https://materialdesignicons.com/)
```js
{
  mobile: {
    forwardAll: 'mdi-chevron-double-down', // Move all items to selected
    forward: 'mdi-chevron-down', // Move selected items to selected
    backwardAll: 'mdi-chevron-double-up', // Move all items to options
    backward: 'mdi-chevron-up' // Move selected items to options
  },
  desktop: {
    forwardAll: 'mdi-chevron-double-right', // Move all items to selected
    forward: 'mdi-chevron-right', // Move selected items to selected
    backwardAll: 'mdi-chevron-double-left', // Move all items to options
    backward: 'mdi-chevron-left' // Move selected items to options
  }
}
```
Default messages
```js
{
  listOptions: 'Options', // First list name
  listSelected: 'Selected', // Second list name
  forwardAll: 'Toggle all to selected', // Tooltip message for move all items to selected
  forward: 'Toggle to selected', // Tooltip message for move selected items to selected
  backwardAll: 'Toggle all to options', // Tooltip message to move all items to options
  backward: 'Toggle to options', // Tooltip message to move selected items to options
  find: 'Find'
}
```
## Work with us!
Feel free to send us an email to [sales@goldenmcorp.com](mailto:sales@goldenmcorp.com)

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.